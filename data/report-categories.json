{
  "client:submission:submit-post-with-content-type": {
    "title": "Client submits activity by sending an HTTP post request to the outbox URL with the Content-Type of application/ld+json; profile=&quot;https://www.w3.org/ns/activitystreams&quot;"
  },
  "client:submission:discovers-url-from-profile": {
    "title": "Client discovers the URL of a user's outbox from their profile"
  },
  "client:submission:submit-objects": {
    "title": "Client submission request body is either a single Activity or a single non-Activity Object"
  },
  "client:submission:submit-objects:provide-object": {
    "title": "Clients provide the object property when submitting the following activity types to an outbox: Create, Update, Delete, Follow, Add, Remove, Like, Block, Undo."
  },
  "client:submission:submit-objects:provide-target": {
    "title": "Clients provide the target property when submitting the following activity types to an outbox: Add, Remove."
  },
  "client:submission:authenticated": {
    "title": "Client sumission request is authenticated with the credentials of the user to whom the outbox belongs"
  },
  "client:submission:recursively-add-targets": {
    "title": "Before submitting a new activity or object, Client infers appropriate target audience by recursively looking at certain properties (e.g. `inReplyTo`, See Section 7), and adds these targets to the new submission's audience."
  },
  "client:submission:recursively-add-targets:limits-depth": {
    "title": "Client limits depth of this recursion."
  },
  "client:retrieval:accept-header": {
    "title": "When retrieving objects, Client specifies an Accept header with the `application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"` media type ([3.2](https://w3c.github.io/activitypub/#retrieving-objects))"
  },
  "outbox:accepts-activities": {
    "title": "Accepts Activity Objects"
  },
  "outbox:accepts-non-activity-objects": {
    "title": "Accepts non-Activity Objects, and converts to Create Activities per 7.1.1"
  },
  "outbox:removes-bto-and-bcc": {
    "title": "Removes the `bto` and `bcc` properties from Objects before storage and delivery"
  },
  "outbox:ignores-id": {
    "title": "Ignores 'id' on submitted objects, and generates a new id instead"
  },
  "outbox:responds-201-created": {
    "title": "Responds with status code 201 Created"
  },
  "outbox:location-header": {
    "title": "Response includes Location header whose value is id of new object, unless the Activity is transient"
  },
  "outbox:update": {
    "title": "Update"
  },
  "outbox:update:partial": {
    "title": "Supports partial updates in client-to-server protocol (but not server-to-server)"
  },
  "outbox:create": {
    "title": "Create"
  },
  "outbox:create:merges-audience-properties": {
    "title": "merges audience properties (to, bto, cc, bcc, audience) with the Create's 'object's audience properties"
  },
  "outbox:create:actor-to-attributed-to": {
    "title": "Create's actor property is copied to be the value of .object.attributedTo"
  },
  "outbox:follow": {
    "title": "Follow"
  },
  "outbox:follow:adds-followed-object": {
    "title": "Adds followed object to the actor's Following Collection"
  },
  "outbox:add": {
    "title": "Add"
  },
  "outbox:add:adds-object-to-target": {
    "title": "Adds object to the target Collection, unless not allowed due to requirements in 7.5"
  },
  "outbox:remove": {
    "title": "Remove"
  },
  "outbox:remove:removes-from-target": {
    "title": "Remove object from the target Collection, unless not allowed due to requirements in 7.5"
  },
  "outbox:like": {
    "title": "Like"
  },
  "outbox:like:adds-object-to-liked": {
    "title": "Adds the object to the actor's Liked Collection."
  },
  "outbox:block": {
    "title": "Block"
  },
  "outbox:block:prevent-interaction-with-actor": {
    "title": "Prevent the blocked object from interacting with any object posted by the actor."
  },
  "outbox:undo": {
    "title": "Supports the Undo activity in the client-to-server protocol"
  },
  "outbox:undo:ensures-activity-and-actor-are-same": {
    "title": "Ensures that the actor in the activity actor is the same in activity being undone."
  },
  "inbox:delivery:performs-delivery": {
    "title": "Performs delivery on all Activities posted to the outbox"
  },
  "inbox:delivery:addressing": {
    "title": "Utilizes `to`, `bto`, `cc`, and `bcc` to determine delivery recipients."
  },
  "inbox:delivery:adds-id": {
    "title": "Provides an `id` all Activities sent to other servers, unless the activity is intentionally transient."
  },
  "inbox:delivery:submit-with-credentials": {
    "title": "Dereferences delivery targets with the submitting user's credentials"
  },
  "inbox:delivery:deliver-to-collection": {
    "title": "Delivers to all items in recipients that are Collections or OrderedCollections"
  },
  "inbox:delivery:deliver-to-collection:recursively": {
    "title": "Applies the above, recursively if the Collection contains Collections, and limits recursion depth >= 1"
  },
  "inbox:delivery:delivers-with-object-for-certain-activities": {
    "title": "Delivers activity with 'object' property if the Activity type is one of Create, Update, Delete, Follow, Add, Remove, Like, Block, Undo"
  },
  "inbox:delivery:delivers-with-target-for-certain-activities": {
    "title": "Delivers activity with 'target' property if the Activity type is one of Add, Remove"
  },
  "inbox:delivery:deduplicates-final-recipient-list": {
    "title": "Deduplicates final recipient list"
  },
  "inbox:delivery:do-not-deliver-to-actor": {
    "title": "Does not deliver to recipients which are the same as the actor of the Activity being notified about"
  },
  "inbox:delivery:do-not-deliver-block": {
    "title": "SHOULD NOT deliver Block Activities to their object."
  },
  "inbox:delivery:sharedInbox": {
    "title": "Delivers to sharedInbox endpoints to reduce the number of receiving actors delivered to by identifying all followers which share the same sharedInbox who would otherwise be individual recipients and instead deliver objects to said sharedInbox."
  },
  "inbox:delivery:sharedInbox:deliver-to-inbox-if-no-sharedInbox": {
    "title": "(For servers which deliver to sharedInbox:) Deliver to actor inboxes and collections otherwise addressed which do not have a sharedInbox."
  },
  "inbox:accept:deduplicate": {
    "title": "Deduplicates activities returned by the inbox by comparing activity `id`s"
  },
  "inbox:accept:special-forward": {
    "title": "Forwards incoming activities to the values of to, bto, cc, bcc, audience if and only if criteria in 7.1.2 are met."
  },
  "inbox:accept:special-forward:recurses": {
    "title": "Recurse through to, bto, cc, bcc, audience object values to determine whether/where to forward according to criteria in 7.1.2"
  },
  "inbox:accept:special-forward:limits-recursion": {
    "title": "Limit recursion in this process"
  },
  "inbox:accept:create": {
    "title": "Supports receiving a Create object in an actor's inbox"
  },
  "inbox:accept:delete": {
    "title": "Assuming object is owned by sending actor/server, removes object's representation"
  },
  "inbox:accept:delete:tombstone": {
    "title": "MAY replace object's representation with a Tombstone object"
  },
  "inbox:accept:update:is-authorized": {
    "title": "Take care to be sure that the Update is authorized to modify its object"
  },
  "inbox:accept:update:completely-replace": {
    "title": "Completely replace its copy of the activity with the newly received value"
  },
  "inbox:accept:dont-blindly-trust": {
    "title": "Don't trust content received from a server other than the content's origin without some form of verification."
  },
  "inbox:accept:follow:add-actor-to-users-followers": {
    "title": "Add the actor to the object user's Followers Collection."
  },
  "inbox:accept:follow:generate-accept-or-reject": {
    "title": "Generates either an Accept or Reject activity with Follow as object and deliver to actor of the Follow"
  },
  "inbox:accept:accept:add-actor-to-users-following": {
    "title": "If in reply to a Follow activity, adds actor to receiver's Following Collection"
  },
  "inbox:accept:reject:does-not-add-actor-to-users-following": {
    "title": "If in reply to a Follow activity, MUST NOT add actor to receiver's Following Collection"
  },
  "inbox:accept:add:to-collection": {
    "title": "Add the object to the Collection specified in the target property, unless not allowed to per requirements in 7.8"
  },
  "inbox:accept:remove:from-collection": {
    "title": "Remove the object from the Collection specified in the target property, unless not allowed per requirements in 7.9"
  },
  "inbox:accept:like:indicate-like-performed": {
    "title": "Perform appropriate indication of the like being performed (See 7.10 for examples)"
  },
  "inbox:accept:announce:add-to-shares-collection": {
    "title": "Increments object's count of shares by adding the received activity to the 'shares' collection if this collection is present"
  },
  "inbox:accept:undo": {
    "title": "Performs Undo of object in federated context"
  },
  "server:inbox:responds-to-get": {
    "title": "Server responds to GET request at inbox URL"
  },
  "server:inbox:is-orderedcollection": {
    "title": "inbox is an OrderedCollection"
  },
  "server:inbox:filtered-per-permissions": {
    "title": "Server filters inbox content according to the requester's permission"
  },
  "server:object-retrieval:get-id": {
    "title": "Allow dereferencing Object `id`s by responding to HTTP GET requests with a representation of the Object"
  },
  "server:object-retrieval:respond-with-as2-re-ld-json": {
    "title": "Respond with the ActivityStreams object representation in response to requests that primarily Accept the media type `application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"`"
  },
  "server:object-retrieval:respond-with-as2-re-activity-json": {
    "title": "Respond with the ActivityStreams object representation in response to requests that primarily Accept the media type `application/activity+json`"
  },
  "server:object-retrieval:deleted-object:tombstone": {
    "title": "Responds with response body that is an ActivityStreams Object of type `Tombstone` (if the server is choosing to disclose that the object has been removed)"
  },
  "server:object-retrieval:deleted-object:410-status": {
    "title": "Respond with 410 Gone status code if Tombstone is in response body, otherwise responds with 404 Not Found"
  },
  "server:object-retrieval:deleted-object:404-status": {
    "title": "Respond with 404 status code for Object URIs that have never existed"
  },
  "server:object-retrieval:private-403-or-404": {
    "title": "Respond with a 403 Forbidden status code to all requests that access Objects considered Private (or 404 if the server does not want to disclose the existence of the object, or another HTTP status code if specified by the authorization method)"
  },
  "server:security-considerations:actually-posted-by-actor": {
    "title": "Server verifies that the new content is really posted by the actor indicated in Objects received in inbox and outbox"
  },
  "server:security-considerations:do-not-post-to-localhost": {
    "title": "By default, implementation does not make HTTP requests to localhost when delivering Activities"
  },
  "server:security-considerations:uri-scheme-whitelist": {
    "title": "Implementation applies a whitelist of allowed URI protocols before issuing requests, e.g. for inbox delivery"
  },
  "server:security-considerations:filter-incoming-content": {
    "title": "Server filters incoming content both by local untrusted users and any remote users through some sort of spam filter"
  },
  "server:security-considerations:sanitize-fields": {
    "title": "Implementation takes care to santizie fields containing markup to prevent cross site scripting attacks"
  }
}