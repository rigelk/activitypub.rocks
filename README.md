# activitypub.rocks

This is the source for activitypub.rocks.

## Dependencies

This site runs with Hugo. See https://gohugo.io/getting-started/installing/

It also requires NodeJS to post-process its assets. Run `npm install` to install the NodeJS development dependencies.

## Development

Make sure you have installed all the above dependencies.

```bash
hugo server -D
```

## Building the site

Make sure you have installed all the above dependencies.

In case your end domain is different from `activitypub.rocks`, set the `baseURL` in the root `config.toml` to reflect your domain.

```bash
npm run build
```

You can now find the generated site in the generated `public` directory. Copy it to your webserver root and you're done!
