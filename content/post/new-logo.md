+++
date = "2017-05-08T22:00:00.000Z"
draft = false
title = "New tutorial, new logo!"
slug = "new-logo"
tags = []
+++

A new Candidate Recommendation of ActivityPub has been released, and this one includes a really nice new feature... a tutorial! It includes some very nice illustrations by mray:

![inbox/outbox relationship from the tutorial](/images/ActivityPub-tutorial-image.png)

Not only that! `mray` has also done us the honor of designing us a sweet new logo:

![New ActivityPub logo](/images/ActivityPub-logo.svg)

Thank you `mray` for making ActivityPub look good!