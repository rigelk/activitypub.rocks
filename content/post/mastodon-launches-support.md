+++
date = "2017-09-09T22:00:00.000Z"
draft = false
title = "Mastodon launches their ActivityPub support, and a new CR!"
slug = "mastodon-launches-support"
tags = []
+++

Big news this week, from multiple directions!

## Mastodon and ActivityPub, together at last!

First of all, [Mastodon](https://joinmastodon.org/) has [launched version 1.6 with ActivityPub support](https://hackernoon.com/mastodon-and-the-w3c-f75f376f422)! This is a _huge_ piece of news.

While Mastodon is not the first implementer of ActivityPub, they are by far the largest existing implementation, with approximately 850k registered users. We're excited to have them on board!

## A new Candidate Recommendation!

This week we published a [new Candidate Recommendation](https://www.w3.org/TR/2017/CR-activitypub-20170907) (aka &quot;CR&quot;) of [ActivityPub](https://www.w3.org/TR/activitypub/). This release contains two major changes:

- <code><a href="https://www.w3.org/TR/activitypub/#follow-activity-inbox">Follow</a></code> requests are now explicitly replied to with either an <code><a href="https://www.w3.org/TR/activitypub/#accept-activity-inbox">Accept</a></code> or <code><a href="https://www.w3.org/TR/activitypub/#reject-activity-inbox">Reject</a></code>.
- <code>publicInbox</code> is now renamed to <code><a href="https://www.w3.org/TR/activitypub/#shared-inbox-delivery">sharedInbox</a></code> and in addition to being used for distributing and reading public posts, it may also be used to deliver posts to followers to reduce the number of HTTP requests made on followers-only posts between federated servers.

These changes are normative, so update your implementations!

The Candidate Recommendation is the period in which we try to stabilize the standard and get enough implementation to make it to Recommendation status. We don't take normative changes lightly, but sometimes we such changes are found to be necessary in this stage... in this case, we found that both of these changes were necessary for Mastodon, and projects the
size of Mastodon, to adopt ActivityPub.

## What's next?

We've quietly launched the <a href="https://test.activitypub.rocks/">test suite</a>... very quietly, because not all test suite functionality is added yet! Only the &quot;client-to-server server&quot; section is yet working. However, we're working on the rest of it and hope to have it up sometime within the next few weeks.

That's it for now. Happy implementing, everyone!