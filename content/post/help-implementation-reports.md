+++
date = "2017-04-08T22:00:00.000Z"
draft = false
title = "Help submit implementation reports!"
slug = "help-implementation-reports"
tags = []
+++

_This was done a while ago, but I forgot to update the website. Oops!_

We're happy to announce that ActivityPub is now open for implementation report submissions! In order for ActivityPub to transition from being a Candidate Recommendation to being a Proposed Recommendation, we need submissions showing that people are using the standard, and help identifying which features are used. (Filling in the implementation report has an added bonus: alongside following the standard, it's a helpful checklist to make sure you're implementing all the things you meant to!)

A huge thank you to [Benjamin Goering](https://bengo.is) for adding the implementation report template, which has helped tremendously! You rock, bengo!

What's coming next? The test suite should be out as soon as possible! So if you start implementing now, you'll be able to run against the test suite very shortly!