+++
date = "2016-11-16T23:00:00.000Z"
draft = false
title = "ActivityPub reaches Candidate Recommendation status!"
slug = "candidate-recommendation"
tags = []
+++

We're thrilled to announce that [ActivityPub](https://www.w3.org/TR/activitypub/) has officially reached [Candidate Recommendation](https://www.w3.org/2005/10/Process-20051014/tr.html#RecsCR) status! This is a huge milestone, signaling that ActivityPub has moved to a point of stability where we're no longer planning major revisions and would like to see as many projects move to implement as possible.

In other words, now's a great time for YOU to start implementing! Head on over to [the standard](https://www.w3.org/TR/activitypub/) and start building!

We're also planning tools to [test](http://test.activitypub.rocks) your implementations soon, and [submit implementation reports](http://activitypub.rocks/implementation-report/). There's a lot to do, but we're on track towards being a proposed recommendation to the W3C!