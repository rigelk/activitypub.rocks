+++
title = "activitypub.rocks launches"
date = "2016-11-13T23:00:00.000Z"
draft = false
slug = "activitypubrocks-launches"
tags = []
image = ""
comments = false # set false to hide Disqus
share = true # set false to hide share buttons
menu= "" # set "main" to add this content to the main menu
author = "Christopher Allan Webber"
+++

Today marks the launch of [activitypub.rocks](https://activitypub.rocks/), the very website you see before you. This will be a combination of an advocacy website for [ActivityPub](https://www.w3.org/TR/activitypub/) as well as tooling to help test and collect feedback on implementations.

There should be more news soon. Hopefully we'll be heading to Candidate Recommendation status shortly, and this very week the [W3C Social Working Group](https://www.w3.org/wiki/Socialwg/) is having another face to face meeting in Boston. So there will be hopefully more to report! In the meanwhile, if you're hungry for news, you may enjoy [this post on the MediaGoblin blog](https://mediagoblin.org/news/tpac-2016-and-review-activitypub.html) about the last Social Working Group face to face meeting.

Onward and upwards towards standardized, federated, decentralized social networks!