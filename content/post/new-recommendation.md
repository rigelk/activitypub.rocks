+++
date = "2018-03-19T23:00:00.000Z"
draft = false
title = "ActivityPub reaches W3C Recommendation status! Everybody party!"
slug = "w3c-recommendation"
tags = []
+++

_Ooops... this post was written months ago, but accidentally wasn't pushed publicly till now! So... a delayed celebration to all!_

{{< tweet 955834312947126273 >}}

We're proud to announce that after a three year journey, [ActivityPub] is now a [W3C Recommendation]! Thank you and congratulations to the many, many people involved in this standardization effort. We couldn't have done it without you.

Now comes the fun part... building ActivityPub applications! Join us in the [Social Web Community Group] to discuss ActivityPub implementation and new frontiers!

[ActivityPub]: https://www.w3.org/TR/activitypub/
[W3C Recommendation]: https://www.w3.org/blog/news/archives/6785
[Social Web Community Group]: https://www.w3.org/wiki/SocialCG