module.exports = {
  plugins: {
    '@fullhuman/postcss-purgecss': {
      content: ['themes/activitypub/layouts/**/*.html', 'themes/activitypub/layouts/*.html'],
      whitelist: []
    }
  }
}

